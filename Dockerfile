FROM registry.gitlab.com/rodrigo.bragaaa/question-core-curso-gitlab:dependencies

COPY . .

RUN mkdir -p assets/static \
    && python manage.py collectstatic --noinput
