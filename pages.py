class BasePage:

    def __init__(self, driver):
        self.driver = driver


class LoginPage(BasePage):

    def login(self, user, password):
        self.driver.find_element_by_name("username").send_keys(user)
        self.driver.find_element_by_name("password").send_keys(password)
        self.driver.find_element_by_name("action").click()


class MainPage(BasePage):

    def access_menu(self, menu_option):
        self.driver.find_element_by_xpath("//h6[contains(.,'" + menu_option + "')]").click()


class MessagePage(BasePage):

    def success_visible(self):
        return self.driver.find_element_by_css_selector(".toast.success-toast").is_displayed()

    def required_visible(self):
        return self.driver.find_element_by_css_selector(".errornote").is_displayed()


class QuestionListPage(BasePage):

    def add_button(self):
        self.driver.find_element_by_xpath("//a[contains(.,'add')]").click()

    def select_remove_all(self):
        self.driver.find_element_by_css_selector(".text span").click()
        self.driver.find_element_by_css_selector(".dropdown-trigger").click()
        self.driver.find_element_by_xpath("//li[contains(., 'Remover questions selecionados')]").click()
        self.driver.find_element_by_name("index").click()
        self.driver.find_element_by_css_selector(".red").click()


class QuestionAddPage(BasePage):

    def add(self, text_question, owner, choice_one, choice_two, choice_three, choice_four):
        self.driver.find_element_by_name("text").send_keys(text_question)
        self.driver.find_element_by_css_selector(".select-dropdown:nth-child(1)").click()
        self.driver.find_element_by_xpath("//span[contains(.,'" + owner + "')]").click()
        self.driver.find_element_by_name("choices-0-text").send_keys(choice_one)
        self.driver.find_element_by_name("choices-1-text").send_keys(choice_two)
        self.driver.find_element_by_name("choices-2-text").send_keys(choice_three)
        self.driver.find_element_by_name("choices-3-text").send_keys(choice_four)
        self.driver.find_element_by_css_selector(".open-actions > button").click()
